/**
 * @file
 * Behaviors file.
 *
 * @see starterkits/default/samples for example implementation
 */

(function ($) {

  Drupal.behaviors.dwsomegaResponsiveNavSearch = {
    attach: function (context, settings) {
      settings = settings || Drupal.settings;

      // Exit function if responsive nav is not enabled.
      if (!settings.dwsomega || !settings.dwsomega.responsivenav || (settings.dwsomega.responsivenav.status == 0)) {
        return;
      }

      /**
       * VARS
       */

      var windowWidth = $(window).width();
      var $searchBlock = $(settings.dwsomega.responsivenav.searchblock).first();
      var $searchBtn = $(settings.dwsomega.responsivenav.searchbtn).first();
      var $menuBtn = $(settings.dwsomega.responsivenav.menubtn).first();
      var $menuBlock = $(settings.dwsomega.responsivenav.menublock).first();
      var $searchAndMenuBtns = $searchBtn.add($menuBtn);
      var $searchAndMenuBlocks = $searchBlock.add($menuBlock);


      /**
       * VISIBILITY FUNCTIONS
       */

      // function to alternate between mobile and desktop nav
      function alternateNav() {
        // we big or little?
        if ($(window).width() < settings.dwsomega.responsivenav.breakpoint) {
          $searchAndMenuBtns.show();
          $searchAndMenuBlocks.hide();
          // add classes and aria attributes for WCAG
          $menuBtn.removeClass('lantern--main-nav--open').attr('tabindex','0').attr('role','button').attr('aria-expanded', 'false').attr('aria-controls', 'lantern--main-nav');
        }
        else {
          $searchAndMenuBtns.hide();
          $searchAndMenuBlocks.show();
          $menuBtn.removeAttr('aria-expanded').removeAttr('aria-controls');
        }
      }

      // function to alternate the visibility of the mobile nav
      function toggleNavVisibility(e) {
        $menuBlock.slideToggle(settings.dwsomega.responsivenav.transitionspeed);
        $searchBlock.hide();
        $menuBtn.toggleClass('lantern--main-nav--open');
        // toggle attribute
        if ($menuBtn.attr('aria-expanded') == 'false') {
          $menuBtn.attr('aria-expanded', 'true');
        }
        else {
          $menuBtn.attr('aria-expanded', 'false');
        }
        e.stopPropagation();
      }


      /**
       *
       */

      // add id attributes for aria attribute to indicate control
      $menuBlock.attr('id', 'lantern--main-nav');

      // toggle the search
      $searchBtn.click(function (e) {
        $searchBlock.slideToggle(settings.dwsomega.responsivenav.transitionspeed);
        $menuBlock.hide();
        e.stopPropagation();
      });

      // toggle the menu's visibility
      $menuBtn.click(function (e) { // on click
        toggleNavVisibility(e);
      });
      $menuBtn.keyup(function (e) {
        if (e.keyCode === 13 || e.keyCode === 32) { // key press for WCAG
          toggleNavVisibility(e);
        }
      });

      // alternate between mobile and desktop nav
      $(window).bind('load', function () {
        alternateNav();
      });
      $(window).bind('resize', function () {
        if ($(window).width() != windowWidth) {
          alternateNav();
        }
      });

    }
  };

})(jQuery);
;
/**
 * @file
 * Behaviors file.
 *
 * @see starterkits/default/samples for example implementation
 */

(function ($) {

  Drupal.behaviors.sidebar_nav_actions = {
    attach: function (context, settings) {

      if ($('.l-region--sidebar-first .block--menu-block').length) {  // change to match your menu blocks selector
        // cache the selector

        var $lvl2 = $('.l-region--sidebar-first .block--menu-block .expanded');

        // find the active trail
        $lvl2.not('.active-trail').find('> a').after('<a href="javascript:void(0)" class="expander is-collapsed" aria-expanded="false">expand</a>');
        $lvl2.has('.active-trail').find('> a').after('<a href="javascript:void(0)" class="expander" aria-expanded="true">collapse</a>');

        // hide not active trail
        $('.expander.is-collapsed').next().hide();

        // add aria attributes to mate each control with its panel
        $lvl2.find('> a:first-child').each(function (i) {
          var idText = ($(this).text().replace(/\s+/g, '') + i); // create a meaningful unique id
          $(this).next().attr('aria-controls', idText);
          $(this).next().next('ul').attr('id', idText);
        });

        // toggle its children
        $('.expander').click(function() {
          var text = $(this).text();
          $(this).text(text == "expand" ? "collapse" : "expand");
          //$(this).next().fadeToggle();

          // update aria properties
          if ($(this).attr('aria-expanded') == 'false') {
            $(this).attr('aria-expanded', 'true');
            $(this).next().fadeIn();
          }
          else {
            $(this).attr('aria-expanded', 'false');
            $(this).next().fadeOut();
          }
          return false;
        });
      }
    }
  };

  Drupal.behaviors.dwso_expander = {
    attach: function () {

      // cache the selector
      var $trigger = $('.dwso-expandable__title--wrapper');

      // hide by default
      $('.dwso-expandable__target').hide();

      // find the active trail
      $trigger.find('h3').before('<a href="javascript:void(0)" class="dwso-expandable__trigger" aria-expanded="false">expand</a>');

      // add aria attributes to mate each control with its panel
      $trigger.find('a:first-child').each(function (i) {
        var idText = ($(this).next().text().replace(/\s+/g, '') + i); // create a meaningful unique id  TOADD to starterkit
        $(this).attr('aria-controls', idText).parent().next().attr('id', idText);
      });

      // toggle its children
      $('.dwso-expandable__trigger').click(function () {
        var text = $(this).text();
        $(this).text(text === 'expand' ? 'collapse' : 'expand');

        // update aria properties
        if ($(this).attr('aria-expanded') === 'false') {
          $(this).attr('aria-expanded', 'true');
          $(this).parent().next().fadeIn();
        }
        else {
          $(this).attr('aria-expanded', 'false');
          $(this).parent().next().fadeOut();
        }
        return false;
      });
    }
  }

  Drupal.behaviors.publications_view_full = {
    attach: function () {
      $('.node-type-publication .field--name-body').before('<a class="view-full">View full document</a>');
      
      $('.view-full').click(function() {
        if ($(this).hasClass("active")) {
          $('.field--name-body').slideUp();
          $(this).text("View full document");
          $(this).removeClass("active");
        } else {
          $('.field--name-body').slideDown();
          $(this).text("Hide full document");
          $(this).addClass("active");
        }
      });
    }
  }

})(jQuery);;
